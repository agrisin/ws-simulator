FROM rodolpheche/wiremock:2.26.3-alpine

COPY stubs /home/wiremock

CMD ["--local-response-templating", "--no-request-journal"]